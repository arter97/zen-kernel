#!/bin/bash

rm -rf /tmp/zen-kernel
mkdir /tmp/zen-kernel
cp -rp .git/ /tmp/zen-kernel/.git
cd /tmp/zen-kernel/
git reset --hard

cp defconfig .config
make "$@"
